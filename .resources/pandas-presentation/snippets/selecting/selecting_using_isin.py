import pandas as pd
import os
import context

data_file = os.path.join(context.data_dir, 'pokemon.csv')

df = pd.read_csv(data_file)


# Selecting up to AND INCLUDING the 5th row and the Type_1 columns
print(df[df['Type_1'].isin(['Grass', 'Fire'])])

