import pandas as pd
import os
import context


data_file = os.path.join(context.data_dir, 'pokemon.csv')


df = pd.read_csv(data_file)

#################################################################
# Group By Multiple Columns
print(df.groupby('Type_1').mean())

#################################################################
# Group By Multiple Columns

# What columns want to look at the mean for
cols = ['Type_1', 'Type_2', 'HP', 'Attack', 'Defense', 'Sp_Atk', 'Sp_Def', 'Speed', 'Height_m', 'Weight_kg', 'Catch_Rate']

print(df[cols].groupby(['Type_1', 'Type_2']).mean())

