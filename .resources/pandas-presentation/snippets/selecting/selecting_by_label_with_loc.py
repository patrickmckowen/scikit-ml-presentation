import pandas as pd
import os
import context


data_file = os.path.join(context.data_dir, 'pokemon.csv')

df = pd.read_csv(data_file)


# Selecting up to AND INCLUDING the 5th row and the Type_1 columns
print(df.loc[:5, 'Type_1'])

# Now lets try setting the index 
df.set_index('Number', inplace=True)
print(df)

# ... and run the last selection command again
print(df.loc[:5, 'Type_1'])


# Use a list to specify specific columns (all rows)
print(df.loc[:, ['Type_1', 'Sp_Atk']])

# Or use a range to select a range of columns (all rows)
print(df.loc[:, 'Type_1':'Sp_Atk'])
