import pandas as pd
import context
import sqlite3
import os

db_file_path = os.path.join(context.data_dir, "chinook.db")


print(db_file_path)

conn = sqlite3.connect(db_file_path)


query = """
SELECT
    trackid,
    tracks.name AS track,
    albums.title AS album,
    artists.name AS artist
FROM
    tracks
    INNER JOIN albums ON albums.albumid = tracks.albumid
    INNER JOIN artists ON artists.artistid = albums.artistid;
"""

df = pd.read_sql_query(query, con=conn)

print(df)
