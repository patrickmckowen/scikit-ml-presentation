import pandas as pd

s = pd.Series([0, 1, 2, 3, 4])
s.replace(0, 5)


df = pd.DataFrame({'A': [0, 1, 2, 3, 4],
                    'B': [5, 6, 7, 8, 9],
                    'C': ['a', 'b', 'c', 'd', 'e']})
df.replace(0, 5)

df.replace([0, 1, 2, 3], 4)

df.replace([0, 1, 2, 3], [4, 3, 2, 1])

s.replace([1, 2], method='bfill')


df.replace({0: 10, 1: 100})


df.replace({'A': 0, 'B': 5}, 100)

df.replace({'A': {0: 100, 4: 400}})


# Regular expression `to_replace`

df = pd.DataFrame({'A': ['bat', 'foo', 'bait'],
                   'B': ['abc', 'bar', 'xyz']})
df.replace(to_replace=r'^ba.$', value='new', regex=True)

df.replace({'A': r'^ba.$'}, {'A': 'new'}, regex=True)

df.replace(regex=r'^ba.$', value='new')

df.replace(regex={r'^ba.$': 'new', 'foo': 'xyz'})


df.replace(regex=[r'^ba.$', 'foo'], value='new')

# Note that when replacing multiple bool or datetime64 objects, the data types in the to_replace parameter must match the data type of the value being replaced:

df = pd.DataFrame({'A': [True, False, True],
                   'B': [False, True, False]})

df.replace({'a string': 'new value', True: False})  # raises

# TypeError: Cannot compare types 'ndarray(dtype=bool)' and 'str'
# This raises a TypeError because one of the dict keys is not of the correct type for replacement.

# Compare the behavior of s.replace({'a': None}) and s.replace('a', None) to understand the peculiarities of the to_replace parameter:

s = pd.Series([10, 'a', 'a', 'b', 'a'])

s.replace({'a': None})

s.replace('a', None)
